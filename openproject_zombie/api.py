import coreapi
from environs import Env
from functools import partial

env = Env()
env.read_env()

HOSTNAME = env('HOSTNAME')
API_USER = env('APIUSER', 'apikey')
API_KEY = env('APIKEY')
API_URL = f'{HOSTNAME}/api/v3'

API_PROJECT_LIST = f'{API_URL}/projects'
API_WORK_PACKAGES = f'{API_URL}/work_packages'
API_WORK_PACKAGE_DETAILS = f'{API_URL}/work_packages/{{0}}'
API_PROJECT_WORK_PACKAGES = f'{API_URL}/projects/{{0}}/work_packages'
API_PROJECT_CATEGORIES = f'{API_URL}/projects/{{0}}/categories'


class API:
    def __init__(self):
        auth = coreapi.auth.BasicAuthentication(API_USER, API_KEY)
        self.client = coreapi.Client(auth=auth)
        self.fetch_hal = partial(self.client.get, format='hal')
        self.document = self.fetch_hal(API_URL)

    def get_projects(self):
        return self.fetch_hal(API_PROJECT_LIST)

    def get_work_packages(self, project_id=None):
        if project_id is None:
            return self.fetch_hal(API_WORK_PACKAGES)
        return self.fetch_hal(API_PROJECT_WORK_PACKAGES.format(project_id))

    def get_work_package(self, work_package_id):
        return self.fetch_hal(API_WORK_PACKAGE_DETAILS.format(work_package_id))

    def get_categories(self, project_id):
        return self.fetch_hal(API_PROJECT_CATEGORIES.format(project_id))
